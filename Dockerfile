FROM python:3

ADD hello_python.py /

CMD [ "python", "./hello_python.py" ]